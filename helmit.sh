#!/usr/bin/env bash

if [ "${1}" == "install" ]; then
    curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh &&\
    chmod 700 get_helm.sh &&\
    ./get_helm.sh --version "${HELM_VERSION}" &&\
    helm version --client && rm ./get_helm.sh &&\
    exit 0

elif [ "${1}" == "script" ]; then
    exit 0

elif [ "${1}" == "deploy" ]; then
    travis_ci_operator.sh github-update self master "
        cd charts_repository &&\
        git checkout ${CI_COMMIT_BRANCH} && git checkout master -- index.yaml && git commit -m. ;\
        helm package ../ckan --version "${CI_COMMIT_TAG}" &&\
        helm package ../efs --version "${CI_COMMIT_TAG}" &&\
        helm package ../traefik --version "${CI_COMMIT_TAG}" &&\
        helm package ../provisioning --version "${CI_COMMIT_TAG}" &&\
        helm repo index --url https://gitlab.com/datopian/tech/infrastructure-charts/-/raw/master/charts_repository/ . &&\
        cd .. &&\
        git stash &&\
        git checkout master &&\
        git stash apply &&\
        git add charts_repository/index.yaml \
                charts_repository/ckan-${CI_COMMIT_TAG}.tgz \
                charts_repository/efs-${CI_COMMIT_TAG}.tgz \
                charts_repository/traefik-${CI_COMMIT_TAG}.tgz \
                charts_repository/provisioning-${CI_COMMIT_TAG}.tgz
    " "upgrade helm chart repo to CKAN chart ${CI_COMMIT_TAG}"
    [ "$?" != "0" ] && exit 1
    exit 0

fi

echo unexpected failure
exit 1
