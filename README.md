Helm charts for provisioning infrastructure on GKE cluster.

## Publish a new release

You will need to created a tagged release and push to gitlab. Gitlab CI will
automatically package and publish new release. Eg
```
vi certmanager/Charts.yaml
> version: 0.0.1 -> 0.0.2

# Create Annotated tag
git add certmanager/Charts.yaml
git tag -m'Bump certmanager version' -a v0.0.2rc-1
git push origin master
```

## Values Reference

- `certmanager.api_key` - API key from cloudflare
- `certmanager.email` - Cloudflare Email
- `certmanager.ingressClass` - Ingress Class used by issuer to issue Certificate
